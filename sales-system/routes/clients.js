const express = require('express');
const router = express.Router();
const db = require('../db');

// Ruta para agregar un cliente
router.post('/add', (req, res) => {
  const { name, cedula, phone, address } = req.body;
  db.query('INSERT INTO clients (name, cedula, phone, address) VALUES (?, ?, ?, ?)', [name, cedula, phone, address], (err, result) => {
    if (err) {
      return res.status(500).send('Error en el servidor');
    }
    res.status(201).send('Cliente agregado');
  });
});

// Ruta para listar todos los clientes
router.get('/list', (req, res) => {
  db.query('SELECT * FROM clients', (err, results) => {
    if (err) {
      return res.status(500).send('Error en el servidor');
    }
    res.status(200).json(results);
  });
});

// Ruta para actualizar un cliente
router.put('/update/:id', (req, res) => {
  const { id } = req.params;
  const { name, cedula, phone, address } = req.body;
  db.query('UPDATE clients SET name = ?, cedula = ?, phone = ?, address = ? WHERE id = ?', [name, cedula, phone, address, id], (err, result) => {
    if (err) {
      return res.status(500).send('Error en el servidor');
    }
    res.status(200).send('Cliente actualizado');
  });
});

// Ruta para eliminar un cliente
router.delete('/delete/:id', (req, res) => {
  const { id } = req.params;
  db.query('DELETE FROM clients WHERE id = ?', [id], (err, result) => {
    if (err) {
      return res.status(500).send('Error en el servidor');
    }
    res.status(200).send('Cliente eliminado');
  });
});

module.exports = router;

