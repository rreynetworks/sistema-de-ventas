const express = require('express');
const router = express.Router();
const db = require('../db');

// Ruta para agregar un vendedor
router.post('/add', (req, res) => {
  const { name } = req.body;
  db.query('INSERT INTO sellers (name) VALUES (?)', [name], (err, result) => {
    if (err) {
      return res.status(500).send('Error en el servidor');
    }
    res.status(201).send('Vendedor agregado');
  });
});

// Ruta para listar todos los vendedores
router.get('/list', (req, res) => {
  db.query('SELECT * FROM sellers', (err, results) => {
    if (err) {
      return res.status(500).send('Error en el servidor');
    }
    res.status(200).json(results);
  });
});

// Ruta para actualizar un vendedor
router.put('/update/:id', (req, res) => {
  const { id } = req.params;
  const { name } = req.body;
  db.query('UPDATE sellers SET name = ? WHERE id = ?', [name, id], (err, result) => {
    if (err) {
      return res.status(500).send('Error en el servidor');
    }
    res.status(200).send('Vendedor actualizado');
  });
});

// Ruta para eliminar un vendedor
router.delete('/delete/:id', (req, res) => {
  const { id } = req.params;
  db.query('DELETE FROM sellers WHERE id = ?', [id], (err, result) => {
    if (err) {
      return res.status(500).send('Error en el servidor');
    }
    res.status(200).send('Vendedor eliminado');
  });
});

module.exports = router;
