const express = require('express');
const router = express.Router();
const db = require('../db');

// Ruta para agregar una venta
router.post('/add', (req, res) => {
  const { client_id, seller_id, sale_date, products } = req.body;

  let total = products.reduce((acc, product) => acc + (product.quantity * product.sale_price), 0);

  db.query('INSERT INTO sales (client_id, seller_id, sale_date, total) VALUES (?, ?, ?, ?)', [client_id, seller_id, sale_date, total], (err, result) => {
    if (err) {
      return res.status(500).send('Error en el servidor');
    }
    
    const saleId = result.insertId;
    const saleDetailsQueries = products.map(product => {
      return new Promise((resolve, reject) => {
        db.query('INSERT INTO sale_details (sale_id, product_id, quantity, sale_price) VALUES (?, ?, ?, ?)', 
          [saleId, product.product_id, product.quantity, product.sale_price], (err, result) => {
          if (err) {
            reject(err);
          } else {
            resolve(result);
          }
        });
      });
    });

    Promise.all(saleDetailsQueries)
      .then(() => res.status(201).send('Venta agregada'))
      .catch(err => res.status(500).send('Error en el servidor'));
  });
});


/*  antes de adicionar vendedor

// Ruta para agregar una venta
router.post('/add', (req, res) => {
  const { client_id, sale_date, products } = req.body;

  let total = products.reduce((acc, product) => acc + (product.quantity * product.sale_price), 0);

  db.query('INSERT INTO sales (client_id, sale_date, total) VALUES (?, ?, ?)', [client_id, sale_date, total], (err, result) => {
    if (err) {
      return res.status(500).send('Error en el servidor');
    }
    
    const saleId = result.insertId;
    const saleDetailsQueries = products.map(product => {
      return new Promise((resolve, reject) => {
        db.query('INSERT INTO sale_details (sale_id, product_id, quantity, sale_price) VALUES (?, ?, ?, ?)', 
          [saleId, product.product_id, product.quantity, product.sale_price], (err, result) => {
          if (err) {
            reject(err);
          } else {
            resolve(result);
          }
        });
      });
    });

    Promise.all(saleDetailsQueries)
      .then(() => res.status(201).send('Venta agregada'))
      .catch(err => res.status(500).send('Error en el servidor'));
  });
});

*/

// Ruta para listar todas las ventas
router.get('/list', (req, res) => {
  db.query('SELECT * FROM sales', (err, results) => {
    if (err) {
      return res.status(500).send('Error en el servidor');
    }
    res.status(200).json(results);
  });
});

// Ruta para obtener los detalles de una venta
router.get('/details/:id', (req, res) => {
  const { id } = req.params;
  db.query('SELECT * FROM sale_details WHERE sale_id = ?', [id], (err, results) => {
    if (err) {
      return res.status(500).send('Error en el servidor');
    }
    res.status(200).json(results);
  });
});

module.exports = router;
