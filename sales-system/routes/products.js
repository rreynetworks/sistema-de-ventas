const express = require('express');
const router = express.Router();
const db = require('../db');

// Ruta para agregar un producto
router.post('/add', (req, res) => {
  const { name, supplier_id, quantity, purchase_price, sale_price, purchase_date } = req.body;
  db.query('INSERT INTO products (name, supplier_id, quantity, purchase_price, sale_price, purchase_date) VALUES (?, ?, ?, ?, ?, ?)', 
    [name, supplier_id, quantity, purchase_price, sale_price, purchase_date], (err, result) => {
    if (err) {
      return res.status(500).send('Error en el servidor');
    }
    res.status(201).send('Producto agregado');
  });
});

// Ruta para listar todos los productos
router.get('/list', (req, res) => {
  db.query('SELECT * FROM products', (err, results) => {
    if (err) {
      return res.status(500).send('Error en el servidor');
    }
    res.status(200).json(results);
  });
});

// Ruta para actualizar un producto
router.put('/update/:id', (req, res) => {
  const { id } = req.params;
  const { name, supplier_id, quantity, purchase_price, sale_price, purchase_date } = req.body;
  db.query('UPDATE products SET name = ?, supplier_id = ?, quantity = ?, purchase_price = ?, sale_price = ?, purchase_date = ? WHERE id = ?', 
    [name, supplier_id, quantity, purchase_price, sale_price, purchase_date, id], (err, result) => {
    if (err) {
      return res.status(500).send('Error en el servidor');
    }
    res.status(200).send('Producto actualizado');
  });
});

// Ruta para eliminar un producto
router.delete('/delete/:id', (req, res) => {
  const { id } = req.params;
  db.query('DELETE FROM products WHERE id = ?', [id], (err, result) => {
    if (err) {
      return res.status(500).send('Error en el servidor');
    }
    res.status(200).send('Producto eliminado');
  });
});

module.exports = router;
