const express = require('express');
const router = express.Router();
const db = require('../db');

// Ruta para agregar un proveedor
router.post('/add', (req, res) => {
  const { name, phone, address, contact } = req.body;
  db.query('INSERT INTO suppliers (name, phone, address, contact) VALUES (?, ?, ?, ?)', [name, phone, address, contact], (err, result) => {
    if (err) {
      return res.status(500).send('Error en el servidor');
    }
    res.status(201).send('Proveedor agregado');
  });
});

// Ruta para listar todos los proveedores
router.get('/list', (req, res) => {
  db.query('SELECT * FROM suppliers', (err, results) => {
    if (err) {
      return res.status(500).send('Error en el servidor');
    }
    res.status(200).json(results);
  });
});

// Ruta para actualizar un proveedor
router.put('/update/:id', (req, res) => {
  const { id } = req.params;
  const { name, phone, address, contact } = req.body;
  db.query('UPDATE suppliers SET name = ?, phone = ?, address = ?, contact = ? WHERE id = ?', [name, phone, address, contact, id], (err, result) => {
    if (err) {
      return res.status(500).send('Error en el servidor');
    }
    res.status(200).send('Proveedor actualizado');
  });
});

// Ruta para eliminar un proveedor
router.delete('/delete/:id', (req, res) => {
  const { id } = req.params;
  db.query('DELETE FROM suppliers WHERE id = ?', [id], (err, result) => {
    if (err) {
      return res.status(500).send('Error en el servidor');
    }
    res.status(200).send('Proveedor eliminado');
  });
});

module.exports = router;
