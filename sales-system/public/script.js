// Funciones para manejar los clientes
document.getElementById('client-form')?.addEventListener('submit', async (event) => {
    event.preventDefault();
    
    const id = document.getElementById('client-id').value;
    const name = document.getElementById('client-name').value;
    const cedula = document.getElementById('client-cedula').value;
    const phone = document.getElementById('client-phone').value;
    const address = document.getElementById('client-address').value;
    
    if (id) {
        // Actualizar cliente
        const response = await fetch(`/clients/update/${id}`, {
            method: 'PUT',
            headers: {
                'Content-Type': 'application/json'
            },
            body: JSON.stringify({ name, cedula, phone, address })
        });
        const data = await response.text();
        alert(data);
    } else {
        // Agregar cliente
        const response = await fetch('/clients/add', {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json'
            },
            body: JSON.stringify({ name, cedula, phone, address })
        });
        const data = await response.text();
        alert(data);
    }
    
    document.getElementById('client-form').reset();
    loadClients();
});

async function loadClients() {
    const response = await fetch('/clients/list');
    const clients = await response.json();
    
    const clientList = document.getElementById('client-list');
    clientList.innerHTML = '';
    
    clients.forEach(client => {
        const li = document.createElement('li');
        li.innerHTML = `ID: ${client.id} - ${client.name} - ${client.cedula} - ${client.phone} - ${client.address} 
        <button onclick="editClient(${client.id}, '${client.name}', '${client.cedula}', '${client.phone}', '${client.address}')">Editar</button>
        <button onclick="deleteClient(${client.id})">Eliminar</button>`;
        clientList.appendChild(li);
    });
}

async function editClient(id, name, cedula, phone, address) {
    document.getElementById('client-id').value = id;
    document.getElementById('client-name').value = name;
    document.getElementById('client-cedula').value = cedula;
    document.getElementById('client-phone').value = phone;
    document.getElementById('client-address').value = address;
}

async function deleteClient(id) {
    if (confirm('¿Estás seguro de eliminar este cliente?')) {
        const response = await fetch(`/clients/delete/${id}`, {
            method: 'DELETE'
        });
        const data = await response.text();
        alert(data);
        loadClients();
    }
}

// Cargar datos iniciales
loadClients();

// Funciones para manejar los proveedores
document.getElementById('supplier-form')?.addEventListener('submit', async (event) => {
    event.preventDefault();
    
    const id = document.getElementById('supplier-id').value;
    const name = document.getElementById('supplier-name').value;
    const phone = document.getElementById('supplier-phone').value;
    const address = document.getElementById('supplier-address').value;
    const contact = document.getElementById('supplier-contact').value;
    
    if (id) {
        // Actualizar proveedor
        const response = await fetch(`/suppliers/update/${id}`, {
            method: 'PUT',
            headers: {
                'Content-Type': 'application/json'
            },
            body: JSON.stringify({ name, phone, address, contact })
        });
        const data = await response.text();
        alert(data);
    } else {
        // Agregar proveedor
        const response = await fetch('/suppliers/add', {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json'
            },
            body: JSON.stringify({ name, phone, address, contact })
        });
        const data = await response.text();
        alert(data);
    }
    
    document.getElementById('supplier-form').reset();
    loadSuppliers();
});

async function loadSuppliers() {
    const response = await fetch('/suppliers/list');
    const suppliers = await response.json();
    
    const supplierList = document.getElementById('supplier-list');
    supplierList.innerHTML = '';
    
    suppliers.forEach(supplier => {
        const li = document.createElement('li');
        li.innerHTML = `ID: ${supplier.id} - ${supplier.name} - ${supplier.phone} - ${supplier.address} - ${supplier.contact} 
        <button onclick="editSupplier(${supplier.id}, '${supplier.name}', '${supplier.phone}', '${supplier.address}', '${supplier.contact}')">Editar</button>
        <button onclick="deleteSupplier(${supplier.id})">Eliminar</button>`;
        supplierList.appendChild(li);
    });
}

async function editSupplier(id, name, phone, address, contact) {
    document.getElementById('supplier-id').value = id;
    document.getElementById('supplier-name').value = name;
    document.getElementById('supplier-phone').value = phone;
    document.getElementById('supplier-address').value = address;
    document.getElementById('supplier-contact').value = contact;
}

async function deleteSupplier(id) {
    if (confirm('¿Estás seguro de eliminar este proveedor?')) {
        const response = await fetch(`/suppliers/delete/${id}`, {
            method: 'DELETE'
        });
        const data = await response.text();
        alert(data);
        loadSuppliers();
    }
}
// Cargar datos iniciales
loadSuppliers();

// Cargar proveedores para el formulario de productos
async function loadSuppliers() {
    const response = await fetch('/suppliers/list');
    const suppliers = await response.json();

    const supplierSelect = document.getElementById('product-supplier');
    supplierSelect.innerHTML = '<option value="">Seleccione un proveedor</option>';
    
    suppliers.forEach(supplier => {
        const option = document.createElement('option');
        option.value = supplier.id;
        option.textContent = supplier.name;
        supplierSelect.appendChild(option);
    });
}

// Funciones para manejar los productos
document.getElementById('product-form')?.addEventListener('submit', async (event) => {
    event.preventDefault();
    
    const id = document.getElementById('product-id').value;
    const name = document.getElementById('product-name').value;
    const supplier_id = document.getElementById('product-supplier').value;
    const quantity = document.getElementById('product-quantity').value;
    const purchase_price = document.getElementById('product-purchase-price').value;
    const sale_price = document.getElementById('product-sale-price').value;
    const purchase_date = document.getElementById('product-purchase-date').value;
    
    if (id) {
        // Actualizar producto
        const response = await fetch(`/products/update/${id}`, {
            method: 'PUT',
            headers: {
                'Content-Type': 'application/json'
            },
            body: JSON.stringify({ name, supplier_id, quantity, purchase_price, sale_price, purchase_date })
        });
        const data = await response.text();
        alert(data);
    } else {
        // Agregar producto
        const response = await fetch('/products/add', {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json'
            },
            body: JSON.stringify({ name, supplier_id, quantity, purchase_price, sale_price, purchase_date })
        });
        const data = await response.text();
        alert(data);
    }
    
    document.getElementById('product-form').reset();
    loadProducts();
});

async function loadProducts() {
    const response = await fetch('/products/list');
    const products = await response.json();
    
    const productList = document.getElementById('product-list');
    productList.innerHTML = '';
    
    products.forEach(product => {
        const li = document.createElement('li');
        li.innerHTML = `ID: ${product.id} - ${product.name} - Proveedor: ${product.supplier_id} - Cantidad: ${product.quantity} - Precio de Compra: ${product.purchase_price} - Precio de Venta: ${product.sale_price} - Fecha de Compra: ${product.purchase_date} 
        <button onclick="editProduct(${product.id}, '${product.name}', ${product.supplier_id}, ${product.quantity}, ${product.purchase_price}, ${product.sale_price}, '${product.purchase_date}')">Editar</button>
        <button onclick="deleteProduct(${product.id})">Eliminar</button>`;
        productList.appendChild(li);
    });
}

async function editProduct(id, name, supplier_id, quantity, purchase_price, sale_price, purchase_date) {
    document.getElementById('product-id').value = id;
    document.getElementById('product-name').value = name;
    document.getElementById('product-supplier').value = supplier_id;
    document.getElementById('product-quantity').value = quantity;
    document.getElementById('product-purchase-price').value = purchase_price;
    document.getElementById('product-sale-price').value = sale_price;
    document.getElementById('product-purchase-date').value = purchase_date;
}

async function deleteProduct(id) {
    if (confirm('¿Estás seguro de eliminar este producto?')) {
        const response = await fetch(`/products/delete/${id}`, {
            method: 'DELETE'
        });
        const data = await response.text();
        alert(data);
        loadProducts();
    }
}

// Cargar datos iniciales
loadSuppliers();
loadProducts();

// Cargar clientes, vendedores y productos para el formulario de ventas
async function loadClientsSellersAndProducts() {
    const clientResponse = await fetch('/clients/list');
    const clients = await clientResponse.json();

    const clientSelect = document.getElementById('sale-client');
    clients.forEach(client => {
        const option = document.createElement('option');
        option.value = client.id;
        option.textContent = client.name;
        clientSelect.appendChild(option);
    });

    const sellerResponse = await fetch('/sellers/list');
    const sellers = await sellerResponse.json();

    const sellerSelect = document.getElementById('sale-seller');
    sellers.forEach(seller => {
        const option = document.createElement('option');
        option.value = seller.id;
        option.textContent = seller.name;
        sellerSelect.appendChild(option);
    });

    const productResponse = await fetch('/products/list');
    const products = await productResponse.json();

    const productSelects = document.querySelectorAll('.sale-product');
    productSelects.forEach(productSelect => {
        products.forEach(product => {
            const option = document.createElement('option');
            option.value = product.id;
            option.textContent = product.name;
            productSelect.appendChild(option);
        });
    });
}

// Función para agregar más productos al formulario de ventas
document?.addEventListener('click', (event) => {
    if (event.target.classList.contains('add-product')) {
        const productContainer = document.getElementById('products-container');
        const productItem = document.createElement('div');
        productItem.className = 'product-item';
        productItem.innerHTML = `
            <select class="sale-product" required>
                <option value="">Seleccione un producto</option>
            </select>
            <input type="number" class="sale-quantity" placeholder="Cantidad" required>
            <input type="number" class="sale-price" placeholder="Precio de Venta" required>
            <button type="button" class="add-product">Agregar Producto</button>
        `;
        productContainer.appendChild(productItem);
        
       // Volver a cargar los productos en el nuevo select
       loadClientsSellersAndProducts();
    }
});

// Funciones para manejar las ventas
document.getElementById('sale-form')?.addEventListener('submit', async (event) => {
    event.preventDefault();
    
    const client_id = document.getElementById('sale-client').value;
    const seller_id = document.getElementById('sale-seller').value;
    const sale_date = document.getElementById('sale-date').value;
    
    const products = [];
    const productItems = document.querySelectorAll('.product-item');
    productItems.forEach(item => {
        const product_id = item.querySelector('.sale-product').value;
        const quantity = item.querySelector('.sale-quantity').value;
        const sale_price = item.querySelector('.sale-price').value;
        products.push({ product_id, quantity, sale_price });
    });

    const response = await fetch('/sales/add', {
        method: 'POST',
        headers: {
            'Content-Type': 'application/json'
        },
        body: JSON.stringify({ client_id, seller_id, sale_date, products })
    });
    const data = await response.text();
    alert(data);

    document.getElementById('sale-form').reset();
    loadSales();
    loadClientsSellersAndProducts(); // Recargar select de productos después de resetear el formulario
});

async function loadSales() {
    const response = await fetch('/sales/list');
    const sales = await response.json();
    
    const saleList = document.getElementById('sale-list');
    saleList.innerHTML = '';
    
    sales.forEach(sale => {
        const li = document.createElement('li');
        li.innerHTML = `ID: ${sale.id} - Cliente: ${sale.client_id} - Vendedor: ${sale.seller_id} - Fecha: ${sale.sale_date} - Total: ${sale.total} 
        <button onclick="viewSaleDetails(${sale.id})">Ver Detalles</button>`;
        saleList.appendChild(li);
    });
}

async function viewSaleDetails(id) {
    const response = await fetch(`/sales/details/${id}`);
    const saleDetails = await response.json();
    
    alert('Detalles de la venta:\n' + saleDetails.map(detail => `Producto: ${detail.product_id}, Cantidad: ${detail.quantity}, Precio de Venta: ${detail.sale_price}`).join('\n'));
}

// Cargar datos iniciales
loadClientsSellersAndProducts();
loadSales();


