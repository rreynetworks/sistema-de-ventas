const express = require('express');
const bodyParser = require('body-parser');
const clientRoutes = require('./routes/clients');
const supplierRoutes = require('./routes/suppliers');
const productRoutes = require('./routes/products');
const saleRoutes = require('./routes/sales');
const sellerRoutes = require('./routes/sellers');

const app = express();

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));
app.use(express.static('public'));

// Rutas
app.use('/clients', clientRoutes);
app.use('/suppliers', supplierRoutes);
app.use('/products', productRoutes);
app.use('/sales', saleRoutes);
app.use('/sellers', sellerRoutes);

// Ruta principal
app.get('/', (req, res) => {
    res.sendFile(__dirname + '/public/index.html');
});

const PORT = 3000;
app.listen(PORT, () => {
    console.log(`Server running on port ${PORT}`);
});

