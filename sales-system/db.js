const mysql = require('mysql');

const connection = mysql.createConnection({
  host: 'localhost',
  user: 'root',  // Cambia esto si tu usuario de MySQL es diferente
  password: 'R00tPasswprd***',  // Cambia esto por tu contraseña de MySQL
  database: 'salesDB'
});

connection.connect((err) => {
  if (err) {
    console.error('Error connecting to MySQL:', err);
    return;
  }
  console.log('Connected to MySQL');
});

module.exports = connection;
